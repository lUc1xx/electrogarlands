import React, {useState} from 'react';
import Head from 'next/head'
import Header from "../com/Header";
import Products from "../com/Products";
import Form from "../com/Form";
import FormModal from "../com/FormModal";
import Advantages from "../com/Advantages";

export default function Home() {
    const [modalOpen, setModalOpen] = useState(false);

    const toggleModal = () => {
        setModalOpen(prev => !prev);
    };

    return (
        <>
            <Head>
                <title>Электрогирлянды интерьерные Черная пятница на новогоднее настроение</title>
                <meta name="description" content="Электрогирлянда качества LUX со скидой до 60%"/>
                <link rel="icon" href="/snowman.ico"/>
            </Head>
                <Header
                    toggleModal={toggleModal}
                />

                <FormModal
                    modalOpen={modalOpen}
                    toggleModal={toggleModal}
                />

                <Advantages/>

                <Products
                    toggleModal={toggleModal}
                />

                <Form/>
        </>
    )
}
