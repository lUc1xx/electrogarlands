import React from 'react';
import s from './Form.module.css';
import FormData from "./FormData";

export default function Form() {
    return (
        <div className={s.wrapper}>

            <FormData/>

            <div className={s.footer}>
                <img src="/images/footer-image.webp" alt=""/>
            </div>
        </div>
    );
}