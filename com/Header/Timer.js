import s from './Timer.module.css';
import React, {useEffect, useState} from "react";

export default function Timer() {
    const [date, setDate] = useState({
        days: "00",
        hours: "00",
        minutes: "00",
        seconds: "00",
    });

    const timer = () => {
        let nowDate = new Date();
        let achiveDate = new Date(2021, 10, 21, 0, 0, 0);
        let result = (achiveDate - nowDate) + 1000;
        let seconds = Math.floor((result / 1000) % 60);
        let minutes = Math.floor((result / 1000 / 60) % 60);
        let hours = Math.floor((result / 1000 / 60 / 60) % 24);
        let days = Math.floor(result / 1000 / 60 / 60 / 24);
        if (days < 10) days = '0' + days;
        if (seconds < 10) seconds = '0' + seconds;
        if (minutes < 10) minutes = '0' + minutes;
        if (hours < 10) hours = '0' + hours;
        setDate({
            ...date,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds
        })
    };

    useEffect(() => {
        const id = setInterval(timer, 1000);
        return () => clearInterval(id);
    }, []);

    return (
        <div className={s.timer}>
            <div className={s.timer__title}>Новогодняя распродажа:</div>
            <div className={s.timer__blocks}>
                <div>
                    <span>{date.days}</span>
                    <span>День</span>
                </div>
                <div>
                    <span>{date.hours}</span>
                    <span>Часы</span>
                </div>
                <div>
                    <span>{date.minutes}</span>
                    <span>Мин</span>
                </div>
                <div>
                    <span>{date.seconds}</span>
                    <span>Сек</span>
                </div>
            </div>
        </div>
    )
}