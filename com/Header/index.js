import s from './Header.module.css'
import Timer from "./Timer";

export default function Header({toggleModal}) {
    return (
        <div className={s.wrapper}>
            <div className={s.container}>
                <button className={s.button} onClick={toggleModal}>купить со скидкой </button>
                <h1 className={s.title}>
                    Электрогирлянды интерьерные <br/> Штора T&E
                </h1>
                <div className={s.subtitle}>
                    Новогоднее настроение <br/>
                    в каждый дом
                </div>
            </div>

            <Timer/>
        </div>
    )
}
