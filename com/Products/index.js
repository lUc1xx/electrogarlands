import s from './Products.module.css'
import Product from "./Product";

export default function Products({toggleModal}) {

    // Описание: Светодиодная, питание от сети 220B // 8 режимов
    const products = [
        {
            id: 1,
            images: [
                '/images/white_1.webp',
                '/images/white_2.webp',
                '/images/white_3.webp',
                '/images/white_4.webp',
            ],
            name: 'Электрогирлянда интерьерная',
            color: 'Белый',
            price: 640,
            oldPrice: 1099,
        },
        {
            id: 3,
            images: [
                '/images/colored_3.webp',
                '/images/colored_2.webp',
                '/images/colored_1.webp',
            ],
            name: 'Электрогирлянда интерьерная',
            color: 'Цветной',
            price: 640,
            oldPrice: 1299,
        },
        {
            id: 2,
            images: [
                '/images/blue_1.webp',
                '/images/blue_2.webp',
                '/images/blue_3.webp',
                '/images/blue_4.webp',
            ],
            name: 'Электрогирлянда интерьерная',
            color: 'Синий',
            price: 640,
            oldPrice: 1199,
        },
        {
            id: 4,
            images: [
                '/images/yellow_1.webp',
                '/images/yellow_2.webp',
                '/images/yellow_3.webp',
                '/images/yellow_4.webp',
            ],
            name: 'Электрогирлянда интерьерная',
            color: 'Желтый',
            price: 640,
            oldPrice: 1099,
        },
    ];

    return (
        <div className={s.wrapper}>
            <div className={s.container}>
                <h2 className={s.title}>Электрогирлянды варианты</h2>
                <span className={s.separator}>
                    <span/>
                </span>
                <div className={s.products}>
                    {products.map((item) => (
                        <Product
                            key={item.id}
                            toggleModal={toggleModal}
                            item={item}
                        />
                    ))}
                </div>
            </div>
        </div>
    )
}
