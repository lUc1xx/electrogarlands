import React from 'react';
import s from './Product.module.css';
import {Navigation, Lazy, A11y, EffectFade} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/lazy';
import 'swiper/css/effect-fade';


export default function Product(props) {
    const {toggleModal} = props;
    const {id, images, name, price, oldPrice, color} = props.item;

    return (
        <div className={s.item}>
            <div className={s.image}>
                <Swiper
                    modules={[Navigation, Lazy, A11y, EffectFade]}
                    effect="fade"
                    spaceBetween={0}
                    slidesPerView={1}
                    height={100}
                    navigation
                    loop={true}
                    lazy={true}
                    onSwiper={(swiper) => console.log(swiper)}
                >
                    {images.map((item, index) => {
                        return (
                            <SwiperSlide key={index} width={260} height={390}>
                                <img
                                    className="swiper-lazy"
                                    width={260}
                                    height={390}
                                    src="/images/loader.gif"
                                    data-src={item}
                                    alt={name}
                                />
                            </SwiperSlide>
                        )
                    })}
                </Swiper>
            </div>
            <div className={s.content}>
                <h4>
                    {name} <br/>
                </h4>
                <div className={s.content__options}>
                    <span>
                        Цвет: {color}
                    </span>
                    <span>
                        Размеры (м): 1.5x1.5, 2x2, 3x2, 3x3
                    </span>
                </div>
                <div className={s.price}>
                    <span className={s.price__sale}>-{Math.round((oldPrice - price) / oldPrice * 100)}%</span>
                    <span className={s.price__actual}>
                        {price}
                        <svg width="13" height="16" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M0 7H1V0H8.5C11 0 13 2 13 4.5C13 7 11 9 8.5 9H3V11H9V13H3V18H1V13H0V11H1V9H0V7ZM8.5 2H3V7H8.5C9.16304 7 9.79893 6.73661 10.2678 6.26777C10.7366 5.79893 11 5.16304 11 4.5C11 3.83696 10.7366 3.20107 10.2678 2.73223C9.79893 2.26339 9.16304 2 8.5 2V2Z"/>
                        </svg>
                    </span>
                    <span className={s.price__old}>
                        {oldPrice}
                        <svg width="10" height="13" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M0 7H1V0H8.5C11 0 13 2 13 4.5C13 7 11 9 8.5 9H3V11H9V13H3V18H1V13H0V11H1V9H0V7ZM8.5 2H3V7H8.5C9.16304 7 9.79893 6.73661 10.2678 6.26777C10.7366 5.79893 11 5.16304 11 4.5C11 3.83696 10.7366 3.20107 10.2678 2.73223C9.79893 2.26339 9.16304 2 8.5 2V2Z"/>
                        </svg>
                    </span>
                </div>
                <span className={s.freeship}>+ бесплатная доставка</span>
                <button className={s.button} onClick={toggleModal}>купить</button>
            </div>
        </div>
    );
}